<?php

return [
    'dev_mode' => true,
    'daemon_mode' => false,
    'target_site' => 'http://ya.ru',
    'intervals' => [180, 600, 3000, 6000, 30000],
    'check_interval' => 30,
    'redis_host' => '127.0.0.1',
    'notifier' => 'mailer',
    'logger' => 'monolog',
    'mailer' => [
        'class' => \Notifiers\Mailer::class,
        'parameters' => [
            'host' => 'smtp.yandex.ru',
            'port' => 465,
            'user' => 'artec-test@yandex.ru',
            'password' => 'qazwsxedcrfv',
            'admin_email' => 'email',
        ]
    ],
    'monolog' => [
        'class' => \Logger::class,
        'parameters' => [
            'error_log' => 'error.log',
        ]
    ],
];

