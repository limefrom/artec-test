<?php

class App
{
    /**
     * @var array
     */
    private static $registry = [];
    
    /**
     * @var Config
     */
    private static $config;
    
    public static function get($key)
    {
        if (!isset(self::$registry[$key])) {
            $config = self::$config;
            $class = $config->get($config->get($key));
            self::$registry[$key] = new $class['class']($class['parameters']);
        }
        if (!is_object(self::$registry[$key])) {
            throw new Exception('Undefined service-alias "' . $key . '"');
        }
        return self::$registry[$key];
    }

    public static function loadConfig(Config $configClass)
    {
        self::$config = $configClass;
    }
    
    public static function getConfig()
    {
        return self::$config;
    }

}