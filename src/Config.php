<?php

/**
 * Created by PhpStorm.
 * User: lime
 * Date: 05.07.16
 * Time: 10:02
 */
class Config
{
    private $config;
    private static $instance = null;

    /**
     * @param array $config
     * @return Config
     */
    public static function load(array $config)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($config);
        }
        return self::$instance;
    }

    /**
     * @param string $key
     * @return bool|string
     */
    public function get($key)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }
        return false;
    }

    private function __construct($config)
    {
        $this->config = $config;
    }

    private function __clone()
    {
    }
}