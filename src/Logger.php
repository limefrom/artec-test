<?php

use Monolog\Logger as Log;
use Monolog\Handler\StreamHandler;

class Logger
{
    private $logger;
    
    public function __construct($parameters)
    {
        $this->logger = new Log('artec');
        $this->logger->pushHandler(new StreamHandler(__DIR__ . '/../' . $parameters['error_log'], Log::ERROR));
    }
    
    public function log($msg)
    {
        $this->logger->error($msg);
    }

}
