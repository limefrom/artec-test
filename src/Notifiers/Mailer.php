<?php

namespace Notifiers;


class Mailer implements NotifierInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Swift_Mime_MimePart
     */
    private $message;

    /**
     * @var string
     */
    private $adminEmail;

    /**
     * Mailer constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $transport = \Swift_SmtpTransport::newInstance($parameters['host'], $parameters['port'], 'ssl')
            ->setUsername($parameters['user'])
            ->setPassword($parameters['password']);
        $this->message = \Swift_Message::newInstance('Red alert')
            ->setFrom($parameters['user'])
            ->setTo($parameters['admin_email']);

        $this->mailer = \Swift_Mailer::newInstance($transport);
        $this->adminEmail = $parameters['admin_email'];
    }

    public function notify($msg)
    {
        $this->message->setBody($msg);
        $result = $this->mailer->send($this->message);
        if (!$result) {
            throw new \Exception('Mailer error: ' . $msg);
        }
    }
}