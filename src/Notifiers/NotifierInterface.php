<?php

namespace Notifiers;


interface NotifierInterface
{
    public function notify($msg);
}