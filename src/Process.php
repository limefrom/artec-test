<?php

class Process
{
    private $intervals = [];
    private $checkInterval;

    /**
     * @param \Redis $redis
     * @param \Notifiers\NotifierInterface $notifier
     */
    public function __construct($redis, $notifier, $intervals, $checkInterval = 30)
    {
        $this->redis = $redis;
        $this->notifier = $notifier;
        $this->checkInterval = $checkInterval;
        $this->intervals = $intervals;
    }

    /**
     * @param string $site
     */
    public function monitor($site)
    {
        $this->resetTime();
        $this->unlock();

        while (1) {
            $result = get_headers($site, 1);

            if (!stripos($result[1], '200 OK')) {

                if (!$this->getTime()) {
                    $this->persistTime();
                }

                if ($this->mayNotify()) {
                    $this->notifier->notify('Site "' . $site . '" is not available now');
                    if (false === next($this->intervals)) {
                        $this->lock();
                    }
                }

            } else {
                if ($this->getTime()) {
                    $this->notifier->notify('Site "' . $site . '" is available now');
                }
                $this->resetTime();
                $this->unlock();
                reset($this->intervals);
            }

            sleep($this->checkInterval);
        }
    }

    private function lock()
    {
        $this->redis->set('artec:last_time:lock', 1);
    }

    private function isLocked()
    {
        return (bool)$this->redis->get('artec:last_time:lock');
    }

    private function unlock()
    {
        $this->redis->set('artec:last_time:lock', 0);
    }

    private function persistTime()
    {
        $this->redis->set('artec:last_time', time());
    }

    private function resetTime()
    {
        $this->redis->set('artec:last_time', 0);
    }

    private function getTime()
    {
        return $this->redis->get('artec:last_time');
    }

    private function mayNotify()
    {
        return (bool) !$this->isLocked() && current($this->intervals) <= time() - $this->getTime();
    }

}