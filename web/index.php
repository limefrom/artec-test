<?php

chdir(__DIR__);

$loader = require __DIR__ . '/../vendor/autoload.php';

$config = require '../config.php';
define('DEV_MODE', $config['dev_mode']);
if (!DEV_MODE && $config['daemon_mode']) {
    ignore_user_abort(true);
}

try {

    App::loadConfig(Config::load($config));

    /** @var \Notifiers\NotifierInterface $notifier */
    $notifier = App::get('notifier');

    $redis = new Redis();
    $redis->connect($config['redis_host']);

    require '../src/Process.php';
    $process = new Process($redis, $notifier, $config['intervals']);
    $process->monitor($config['target_site']);

} catch (Exception $e) {
    if (DEV_MODE) {
        echo $e->getMessage() . PHP_EOL;
        debug_print_backtrace();
    } else {
        echo 'Some error occurred!';
    }
    /** @var \Logger $logger */
    $logger = App::get('logger');
    $logger->log($e->getMessage() . PHP_EOL . implode(PHP_EOL, debug_backtrace()));
}